const http = require("http");
const Koa = require("koa");
const bodyparser = require("koa-bodyparser");
const cors = require("@koa/cors");

require("dotenv").config();
const app = new Koa();
app.use(bodyparser());
app.use(cors());

const PORT = process.env.PORT;

const mongoDB = require("./src/db/index");
const Database = mongoDB.db("Todoapp");

module.exports = { mongoDB, Database };

app.use(require("./src/router/user").routes());
app.use(require("./src/router/admin").routes());

http.createServer(app.callback()).listen(PORT, (err) => {
  if (err) {
    console.log("Error occured on starting the server");
    console.log(err);
    return;
  }
  console.log(`Server running successfully on port: ${PORT}`);
});
