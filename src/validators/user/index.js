//eslint-disable-next-line
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
const phoneNumberRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
const { findUserByEmail, sharedUser } = require("../../db/query/users");
const { CheckPassword } = require("../../shared/password");

const isEmailExist = async (ctx, next) => {
  const { email, password } = ctx.request.body;

  if (email) {
    if (password) {
      const findEmail = await findUserByEmail(email);
      if (findEmail) {
        const passwordCheck = await CheckPassword(password, findEmail.password);
        if (passwordCheck) {
          ctx.state.shared = await sharedUser(findEmail.userId);
          return next();
        } else {
          ctx.status = 401;
          ctx.body = { message: "Please enter currect password" };
        }
      } else {
        ctx.status = 401;
        ctx.body = { message: "No user found" };
      }
    } else {
      ctx.status = 401;
      ctx.body = { message: "Please enter password" };
    }
  } else {
    ctx.status = 401;
    ctx.body = { message: "Please enter email address" };
  }
};

const isUserAlreadyExist = async (ctx, next) => {
  const email = ctx.request.body.email;
  if (email) {
    const findEmail = await findUserByEmail(email);
    if (findEmail) {
      ctx.status = 401;
      ctx.body = { message: "User Already Registered" };
    } else {
      return next();
    }
  } else {
    ctx.status = 401;
    ctx.body = { message: "Please enter email address" };
  }
};

const errorValidation = async (ctx, next) => {
  const { fname, lname, email, password, phone, type } = ctx.request.body;

  if (!fname) {
    ctx.status = 401;
    ctx.body = { message: "Please enter first name" };
  } else if (!lname) {
    ctx.status = 401;
    ctx.body = { message: "Please enter last name" };
  } else if (!email) {
    ctx.status = 401;
    ctx.body = { message: "Please enter email address" };
  } else if (!emailRegex.test(email)) {
    ctx.status = 401;
    ctx.body = { message: "Please enter valid address" };
  } else if (!password) {
    ctx.status = 401;
    ctx.body = { message: "Please enter password" };
  } else if (!passwordRegex.test(password)) {
    ctx.status = 401;
    ctx.body = {
      message:
        "Password Must be UpperCase, LowerCase, 1 Special Character AND 8 Characters",
    };
  } else if (!phone) {
    ctx.status = 401;
    ctx.body = { message: "Please enter mobile number" };
  } else if (!phoneNumberRegex.test(phone)) {
    ctx.status = 401;
    ctx.body = {
      message: "Please Enter Proper Number",
    };
  } else if (!type) {
    ctx.status = 401;
    ctx.body = { message: "Please enter type" };
  } else if (!["developer", "tester"].includes(type)) {
    ctx.status = 401;
    ctx.body = { message: "Please enter valid type" };
  } else {
    return next();
  }
};

module.exports = { isEmailExist, isUserAlreadyExist, errorValidation };
