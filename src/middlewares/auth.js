const jwt = require("jsonwebtoken");
const { finduser } = require("../db/query/users");
require("dotenv").config();

const auth = async (ctx, next) => {
  const token = ctx.request.headers.Authorization;

  if (token) {
    try {
      const person = await jwt.verify(token, process.env.SECRET_KEY);
      if (person) {
        const authorizedUser = await finduser(person.id);
        if (authorizedUser) {
          ctx.state.user = authorizedUser;
          return next();
        } else {
          ctx.status = 401;
          ctx.body = { message: "Invalid User" };
        }
      } else {
        ctx.status = 401;
        ctx.body = { message: "Unauthorized person" };
      }
    } catch (e) {
      ctx.status = 401;
      ctx.body = { message: "Enter Proper Token" };
    }
  } else {
    ctx.status = 401;
    ctx.body = { message: "Please Enter Token" };
  }
};

module.exports = auth;
