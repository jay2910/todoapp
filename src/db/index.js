require("dotenv").config();
const { MongoClient } = require("mongodb");

const url = process.env.MONGODB_URL;
const mongoConnection = new MongoClient(url);
mongoConnection
  .connect()
  .then(() => {
    console.log("Database Connected");
  })
  .catch((err) => {
    console.log(err);
  });

module.exports = mongoConnection;
