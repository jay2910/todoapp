const Router = require("koa-router");

const getUserData = require("../controller/admin");

const routers = new Router({
  prefix: "/admin",
});

routers.get("/userList", getUserData);

module.exports = routers;
